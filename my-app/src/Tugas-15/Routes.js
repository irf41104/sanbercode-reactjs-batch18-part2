import React from "react";
import {
    BrowserRouter as Router,
    Switch, 
    Route,
    Link
} from "react-router-dom";
import FruitForm from "../Tugas-9/tugas-9"
import FruitInfo from "../Tugas-10/tugas-10"
import Timer from "../Tugas-11/tugas-11"
import DaftarBuah from "../Tugas-12/daftar-buah"
import DaftarBuahan from "../Tugas-13/daftar-buah"
import Main from "../Tugas-14/main"
import './Routes.css'
import ThemeMode from './ThemeMode'

const Routes = () => {

    return (
        <Router>
          <div>
            <nav>
              <ul>
                <li><Link to="/" className="active">Home</Link></li>
                <li><Link to="/materi-10">Materi 10</Link></li>
                <li><Link to="/materi-11">Materi 11</Link></li>
                <li><Link to="/materi-12">Materi 12</Link></li>
                <li><Link to="/materi-13">Materi 13</Link></li>
                <li><Link to="/materi-14">Materi 14</Link></li>
                <li><ThemeMode/></li>
              </ul>
            </nav>
            
    
            <Switch>
              <Route exact path="/" component= {FruitForm}/>
              <Route exact path="/materi-10" component= {FruitInfo}/>
              <Route exact path="/materi-11" component= {Timer}/>
              <Route exact path="/materi-12" component= {DaftarBuah}/>
              <Route exact path="/materi-13" component= {DaftarBuahan}/>
              <Route exact path="/materi-14" component= {Main}/>
            </Switch>
          </div>
        </Router>
      );
    }

export default Routes;