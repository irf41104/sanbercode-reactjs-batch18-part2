import React from "react";
import "./Routes.css"

const ThemeMode = () =>{


    return(
        <>
            <div style = {{marginLeft: "300px"}}>
                <br/>
                <button className = "buttheme" style={{background: "black", "border-radius": "20px"}}>
                    <a href="#" style={{"text-decoration": "none", color: "white"}}>Dark</a>
                </button>
                <button className = "buttheme" style={{background: "white", "border-radius": "20px"}}>
                    <a href="#" style={{"text-decoration": "none", color: "black"}}>Light</a>
                </button>
            </div>
        </>
    )
}

export default ThemeMode;
