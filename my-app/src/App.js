import React from 'react';
// import logo from './logo.svg';
import './App.css';
// import FruitForm from './Tugas-9/tugas-9';
// import FruitInfo from './Tugas-10/tugas-10';
// import Timer from './Tugas-11/tugas-11';
// import DaftarBuah from './Tugas-13/daftar-buah';
import { BrowserRouter as Router } from "react-router-dom";
import Main from './Tugas-14/main';
import Routes from './Tugas-15/Routes';

function App() {
  return(
    <>
      {/* <FruitForm/>
      <FruitInfo/>
      <Timer/> */}
      {/* <DaftarBuah/> */}
      {/* <Main/> */}
      <Router>
        <Routes/>
      </Router>
    </>
  )
}

export default App;
