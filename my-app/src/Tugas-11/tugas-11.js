import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 100,
      showTime: true,
      clock: new Date()
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    } 
    this.timerID = setInterval(
      () => this.updating(),
      1000
    );
  }

  componentDidUpdate() {
    if (this.state.time === 0 && this.state.showTime){
        this.stopTime();
        this.hideTime();
      }
  }

  componentWillUnmount(){
      clearInterval(this.timerID);
  }

  stopTime(){
      this.componentWillUnmount();
  }

  hideTime(){
      this.setState({showTime: false})
  }

  updating() {
    this.setState({
      time: this.state.time - 1 ,
      clock: new Date()
    });
  }


  render(){
    return(
      <>
        {
            this.state.showTime && ( 
                <h1 style={{textAlign: "center", padding: "40px"}}>
                    sekarang jam: {this.state.clock.toLocaleTimeString("en-US")} &nbsp; hitung mundur: {this.state.time}
                </h1>
            )
        }
      </>
    )
  }
}

export default Timer