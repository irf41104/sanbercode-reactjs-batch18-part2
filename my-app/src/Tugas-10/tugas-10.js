import React from 'react';


let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]

class ShowName extends React.Component {
  render() {
    return <p>{this.props.nama}</p>;
  }
}

class ShowPrice extends React.Component {
    render() {
      return <p>{this.props.harga}</p>;
    }
  }

class ShowWeight extends React.Component {
    render() {
      return <p>{this.props.berat/1000} kg</p>;
    }
  }

class FruitInfo extends React.Component {
    render(){
        return(
            <div className="form">
                <h1 style = {{textAlign : "center", marginTop : "40px"}}>Tabel Harga Buah</h1>
                <table className = "table">
                     <thead className = "thead">
                            <tr>
                                <td>Nama</td>
                                <td>Harga</td>
                                <td>Berat</td>
                            </tr>
                    </thead>
                {dataHargaBuah.map(el=> {
                    return (
                        <tbody className = "tbody">
                            <tr>
                                <td><ShowName nama = {el.nama}/></td>
                                <td><ShowPrice harga = {el.harga}/></td>
                                <td><ShowWeight berat = {el.berat}/></td>
                            </tr>
                        </tbody>
                    )
                }
                )
                }
                </table>
            </div>
        )
    }
}

export default FruitInfo;