import React, {useState, useEffect, createContext} from "react";
import axios from 'axios';

export const FruitListContext = createContext();
export const FruitListProvider =  props => {

    const [fruitList, setFruitList] =  useState(null);;
        
    useEffect ( () => {
        if (fruitList === null){
        axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then(res => {
           setFruitList(res.data.map(el => {
              return {
                id: el.id,
                name: el.name,
                price: el.price,
                weight: el.weight
              }
            }))
          })
        }}, [fruitList])

        const [input, setInput] = useState({name: "",price: "", weight: 0, id: null});

        return (
            <FruitListContext.Provider value = {[fruitList, setFruitList, input, setInput]}>
                {props.children}
            </FruitListContext.Provider>
    );
}