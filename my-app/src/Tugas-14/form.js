import React, {useState, useContext} from "react";
import {FruitListContext} from "./context";
import axios from 'axios'

const FruitListForm = () => {
    const [fruitList, setFruitList, input, setInput] = useContext(FruitListContext);

    const handleSubmit = (event) =>{
      event.preventDefault()
  
      if (input.id === null){
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {
          name: input.name, 
          price: input.price.toString(),
          weight: input.weight})
        .then(res => {
          setFruitList([
            ...fruitList, 
            {
            id: res.data.id,
            name: input.name,
            price: input.price.toString(),
            weight: input.weight
            }
          ])
        })
      
      }else{
          axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {
          name: input.name,
          price: input.price.toString(),
          weight: input.weight
        })
        .then(res => {
          let fruitData = fruitList.map(el => {
            if (el.id === input.id){
              el.name = input.name;
              el.price = input.price.toString();
              el.weight = input.weight;
            }
            return el
        })
    setFruitList(fruitData);
  })}
setInput({name: "", price: "", weight: 0, id: null})
}


const handleChange = (event) => {
    let typeOfInput = event.target.name
    
    switch (typeOfInput){
      case "name": {
          setInput({...input, name: event.target.value});
          break;
        }
      case "price": {
          setInput({...input, price: event.target.value});
          break;
        }
          case "weight": {
          setInput({...input, weight: event.target.value});
          break;
        }
        default: {break;}
      }
  }

return(
    <div style={{margin: "0 auto"}}>
        <form onSubmit={handleSubmit}>
            <h3 style = {{marginBottom: "40px"}}>Masukkan Data Berikut!</h3>
        <label>
            Nama Buah:
        </label>          
        <input type="text" name = "name" style = {{marginLeft: "85px", marginBottom: "20px"}} required onChange = {handleChange} value={input.name} />
        <br/>
        <label>
            Harga Buah:
        </label>          
        <input type="text" name = "price" style = {{marginLeft: "85px" ,marginBottom: "20px"}} required onChange = {handleChange} value={input.price} />
        <br/>
        <label>
            Berat Buah (g):
        </label>          
        <input type="number"  name = "weight" style = {{marginLeft: "65px"}} required onChange={handleChange} value = {input.weight} />
        <br/>
        <br/>

        <button style = {{marginLeft: "270px"}}>Submit</button>
        </form>
    </div>
    )
}

export default FruitListForm;
