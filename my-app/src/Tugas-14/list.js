import React, {useContext} from "react"
import {FruitListContext} from "./context"
import axios from 'axios'

const FruitListList = () => {
    const [fruitList, setFruitList, input, setInput] = useContext(FruitListContext);

    const handleDelete = (event) => {
        let fruitListId = parseInt(event.target.value);
        let newFruitList = fruitList.filter (el => el.id !== fruitListId)
        
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${fruitListId}`)
        .then(res => {
          setFruitList(newFruitList)
        })
      }
    
    const handleEdit = (event) => {
        let fruitListId = parseInt(event.target.value)
        let fruitData = fruitList.find(x => x.id === fruitListId)
        
        setInput({
          name: fruitData.name,
          price: fruitData.price,
          weight: fruitData.weight,
          id: fruitListId
        })
      }

      return(
        <div style={{margin: "0 auto"}}>
            <h1 style={{textAlign: 'center'}}>Tabel Harga Buah</h1>
            <table>
            <thead>
                <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Aksi</th>
                </tr>
            </thead>
              <tbody>
                {
                    fruitList !== null && fruitList.map((item, index)=>{
                    var number = index+1; 
                    return(                    
                        <tr key={number}>
                        <td style={{paddingLeft: "40px"}}>{item.name}</td>
                        <td style={{paddingLeft: "120px"}}>{item.price}</td>
                        <td style={{paddingLeft: "170px"}}>{item.weight/1000} kg</td>
                        <td>
                            <div style = {{textAlign:"center"}}>
                            <button value = {item.id} onClick={handleEdit}>Edit</button>
                            <button style = {{marginLeft: "1em"}} value={item.id} onClick = {handleDelete}>Delete</button>
                            </div>
                        </td>
                        </tr>
                    )
                    })
                }
              </tbody>
            </table>
        </div>
        )
}

export default FruitListList;