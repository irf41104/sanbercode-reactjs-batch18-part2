import React from "react"
import { FruitListProvider } from './context';
import FruitListForm from "./form";
import FruitListList from './list';


function Main () {
    return(
        <>
            <FruitListProvider>
                <FruitListList/>
                <br/>
                <FruitListForm/>
            </FruitListProvider>
        </>
    )
}

export default Main;