import React, {Component} from "react"

class DaftarBuah extends Component{

   
  constructor(props){
    super(props)
    this.state ={
     fruitList : [
        {name: "Semangka", price: 10000, weight: 1000},
        {name: "Anggur", price: 40000, weight: 500},
        {name: "Strawberry", price: 30000, weight: 400},
        {name: "Jeruk", price: 30000, weight: 1000},
        {name: "Mangga", price: 30000, weight: 500}
     ],
     inputName: "",
     inputPrice: 0,
     inputWeight: 0,
     index: -1  
    }
    // bind in constructor
    this.handleEdit = this.handleEdit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  // method with normal function
  handleSubmit(event){
    event.preventDefault()

    let index = this.state.index
    let fruitList = this.state.fruitList
    let name = this.state.inputName
    let price = this.state.inputPrice
    let weight = this.state.inputWeight

    if (name.replace(/\s/g,'') !== "" && price.replace(/\s/g,'') !== ""){      
  
    // if index -1 means submit create
    if (index === -1){
      this.setState({fruitList: [...fruitList, {name, price, weight}]})
    }else{
      // means submit edit
      fruitList[index] = {name,price, weight}

      this.setState({
          fruitList, 
          inputFruit: "",
          inputPrice: 0,
          inputWeight: 0
        })
    }
  }
}



  handleEdit(event){
    let index = event.target.value;
    let fruitList  = this.state.fruitList[index];
    this.setState({
        inputName: fruitList.name,
        inputPrice: fruitList.price,
        inputWeight: fruitList.weight,
        index: index
    })
  }

    handleChange (event){
        let typeOfInput = event.target.name
        switch (typeOfInput){
          case "name":
          {
            this.setState({inputName: event.target.value});
            break
          }
          case "price":
          {
            this.setState({inputPrice: event.target.value});
            break
          }
          case "weight":
          {
            this.setState({inputWeight: event.target.value});
              break
          }
        default:
          {break;}
        }
    
    }
    

    handleDelete = (event) =>{
        let index = event.target.value;
        this.state.fruitList.splice(index, 1)
        this.setState({fruitList: this.state.fruitList})
    }

    render(){
        return(
        <div style={{margin: "0 auto", width: "80%"}}>
            <h1 style={{textAlign: 'center'}}>Tabel Harga Buah</h1>
            <table>
            <thead>
                <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                {
                    this.state.fruitList.map((item, index)=>{
                    var number = index+1; 
                    return(                    
                        <tr key={number}>
                        <td>{item.name}</td>
                        <td>{item.price}</td>
                        <td>{item.weight/1000} kg</td>
                        <td>
                            <div style = {{textAlign:"center"}}>
                            <button  value={index} onClick={this.handleEdit}>Edit</button>
                            <button style={{marginLeft: "1em"}} value={index} onClick={this.handleDelete}>Delete</button>
                            </div>
                        </td>
                        </tr>
                    )
                    })
                }
            </tbody>
            </table>
            <br/>
            <form onSubmit={this.handleSubmit}>
                <h3>Masukkan Data Berikut!</h3>
            <label>
                Nama Buah:
            </label>          
            <input type="text" name = "name" style = {{marginLeft: "85px", marginBottom: "20px"}} required onChange={this.handleChange} value={this.state.inputName} />
            <br/>
            <label>
                Harga Buah:
            </label>          
            <input type="number" name = "price" style = {{marginLeft: "85px" ,marginBottom: "20px"}} required onChange={this.handleChange} value={this.state.inputPrice} />
            <br/>
            <label>
                Berat Buah (g):
            </label>          
            <input type="number"  name = "weight" style = {{marginLeft: "65px"}} required onChange={this.handleChange} value={this.state.inputWeight} />
            <br/>
            <br/>

            <input type="submit" value="Submit" style = {{marginLeft: "270px"}}/>
            </form>
        </div>
        )
    }
}

export default DaftarBuah;