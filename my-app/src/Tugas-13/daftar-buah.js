import React, {useState, useEffect} from "react";
import axios from 'axios';


const DaftarBuahan = () => {
const [fruitList, setFruitList] =  useState(null);
const [input, setInput] = useState({name: "",price: "", weight: 0, id: null});


useEffect ( () => {
  if (fruitList === null){
  axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
  .then(res => {
    setFruitList(res.data.map(el => {
      return {
        id: el.id,
        name: el.name,
        price: el.price,
        weight: el.weight
      }
    }))
  })
}}, [fruitList])

const handleSubmit = (event) =>{
      event.preventDefault()
  
      if (input.id === null){
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {
          name: input.name, 
          price: input.price.toString(),
          weight: input.weight})
        .then(res => {
          setFruitList([
            ...fruitList, 
            {
            id: res.data.id,
            name: input.name,
            price: input.price.toString(),
            weight: input.weight
            }
          ])
        })
      
      }else{
          axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {
          name: input.name,
          price: input.price.toString(),
          weight: input.weight
        })
        .then(res => {
          let fruitData = fruitList.map(el => {
            if (el.id === input.id){
              el.name = input.name;
              el.price = input.price.toString();
              el.weight = input.weight;
            }
            return el
        })
    setFruitList(fruitData);
  })
}
setInput({name: "", price: "", weight: 0, id: null})
}

  const handleEdit = (event) => {
    let fruitListId = parseInt(event.target.value)
    let fruitData = fruitList.find(x => x.id === fruitListId)
    setInput({
      name: fruitData.name,
      price: fruitData.price,
      weight: fruitData.weight,
      id: fruitListId
    })
  }

  const handleChange = (event) => {
      let typeOfInput = event.target.name
      
      switch (typeOfInput){
        case "name": {
            setInput({...input, name: event.target.value});
            break;
          }
        case "price": {
            setInput({...input, price: event.target.value});
            break;
          }
            case "weight": {
            setInput({...input, weight: event.target.value});
            break;
          }
          default: {break;}
        }
    }
  
  const handleDelete = (event) => {
    let fruitListId = parseInt(event.target.value);
    let newFruitList = fruitList.filter (el => el.id !== fruitListId)
    
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${fruitListId}`)
    .then(res => {
      setFruitList(newFruitList)
    })
  }
  

      return(
        <div style={{margin: "0 auto"}}>
            <h1 style={{textAlign: 'center'}}>Tabel Harga Buah</h1>
            <table>
            <thead>
                <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Aksi</th>
                </tr>
            </thead>
              <tbody>
                {
                    fruitList !== null && fruitList.map((item, index)=>{
                    var number = index+1; 
                    return(                    
                        <tr key={number}>
                        <td style={{paddingLeft: "40px"}}>{item.name}</td>
                        <td style={{paddingLeft: "120px"}}>{item.price}</td>
                        <td style={{paddingLeft: "170px"}}>{item.weight/1000} kg</td>
                        <td>
                            <div style = {{textAlign:"center"}}>
                            <button value = {item.id} onClick={handleEdit}>Edit</button>
                            <button style = {{marginLeft: "1em"}} value={item.id} onClick = {handleDelete}>Delete</button>
                            </div>
                        </td>
                        </tr>
                    )
                    })
                }
              </tbody>
            </table>
            <br/>
            <form onSubmit={handleSubmit}>
                <h3 style = {{marginBottom: "40px"}}>Masukkan Data Berikut!</h3>
            <label>
                Nama Buah:
            </label>          
            <input type="text" name = "name" style = {{marginLeft: "85px", marginBottom: "20px"}} required onChange = {handleChange} value={input.name} />
            <br/>
            <label>
                Harga Buah:
            </label>          
            <input type="text" name = "price" style = {{marginLeft: "85px" ,marginBottom: "20px"}} required onChange = {handleChange} value={input.price} />
            <br/>
            <label>
                Berat Buah (g):
            </label>          
            <input type="number"  name = "weight" style = {{marginLeft: "65px"}} required onChange={handleChange} value = {input.weight} />
            <br/>
            <br/>

            <button style = {{marginLeft: "270px"}}>Submit</button>
            </form>
        </div>
        )
}


export default DaftarBuahan;